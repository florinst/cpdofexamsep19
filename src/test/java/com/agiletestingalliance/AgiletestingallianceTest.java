package com.agiletestingalliance;

import static org.junit.Assert.*;
import java.io.*;
import org.junit.Test;

import com.agiletestingalliance.AboutCPDOF;
import com.agiletestingalliance.Duration;
import com.agiletestingalliance.MinMax;
import com.agiletestingalliance.Usefulness;


public class AgiletestingallianceTest {
 
    @Test
    public void runAboutCPDOF() throws Exception {

 	AboutCPDOF aboutCPDOF = new AboutCPDOF();
	String desc = aboutCPDOF.desc();        
	assertTrue(desc.contains("CP-DOF certification program"));
    } 

    @Test
    public void runDuration() throws Exception {

 	Duration duration = new Duration();
	String desc = duration.dur();        
	assertTrue(desc.contains("CP-DOF is designed specifically"));
    } 

    @Test
    public void runMinMax() throws Exception {

 	MinMax max = new MinMax();
	int result = max.max(5,6);        
	assertEquals("minMax1",result,6);
	result = max.max(6,5);
	assertEquals("minMax2",result,6);
    } 

    @Test
    public void runUsefulness() throws Exception {

 	Usefulness usefulness = new Usefulness();
	String desc = usefulness.desc();        
	assertTrue(desc.contains("DevOps is about transformation"));
    } 



}
